const express = require("express");
const { productController } = require("../controllers");
const { refreshToken } = require("../controllers/refreshToken");
const { getUser, Register, Login, Logout, Profile } = require("../controllers/userController");
const { verifyToken } = require("../middleware/VerifyToken");
const user = require("../models/user");

function apply(app) {
  app.post("/api/user/register", Register);
  app.post("/api/user/login", Login);
  app.get("/api/user/token", refreshToken);
  app.delete("/api/user/logout", Logout);
  
  //app.put("/api/user/:id")
  app.get("/api/user/data", verifyToken, getUser);
  app.put("/api/user/:id", Profile); 
  app.get("/api/products", productController.getProducts ); 
  app.post("/api/product/create", productController.createProduct)
  app.put("/api/product/update/:id", productController.updateProduct)
  app.delete("/api/product/remove/:id", productController.deleteProduct)
  // app.get("/api/product/:id", productController.showProduct) 
  return app;
}

module.exports = { apply }
