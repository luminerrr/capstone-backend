const db = require('../models')
const {Products} = require('../models')

const tbl_product = db.tbl_product


module.exports = {
    
async getProducts(req, res) {
    try {
        const product = await Products.findAll();
        res.status(200).json({
            product
        });
    } 
    catch (error) {
        res.status(422).json({
            error,
            data: null,
            "message": "Failed to get Product list"
        })
    }
},

async  getProductById(req, res) {
    try {
        const product = await Products.findByPk(req.params.id, {
            indlude:[{
                model:User,
                as: 'users' 
            }]
        });
        res.status(200).json(product);
    } 
    catch (error) {
        res.status(422).json({
            error,
            data: null,
            "message": "Failed to get Product"
        })
    }
},

async createProduct(req,res){
    try {
        Products.create({
            name: req.body.title,
            type : req.body.type,
            description: req.body.description,
            price : req.body.price,
            user_id : req.body.user_id,
            inventory_id : req.body.inventory_id,
            status : req.body.status
        }).then((newProduct)=>{
            res.status(200).json({
                status : "ok",
                data : {newProduct}
            })
        }).catch((err)=>{
            res.status(400).json({
                status:"error",
                message : err
            })
        })
        
    } 
    catch (error) {
        res.status(402).json({
            error,
            data: null,
            "message":"failed to create"
        })
        
    }
},

async updateProduct(req,res){
    try {
        const product = await Products.findByPk(req.params.id);
        product.update({
            name: req.body.title,
            type : req.body.type,
            description: req.body.description,
            price : req.body.price,
            user_id : req.body.user_id,
            inventory_id : req.body.inventory_id,
            status : req.body.status
        })
        
    } 
    catch (error) {
        res.status(402).json({
            error,
            data: null,
            "message":"failed to create"
        })
    }
},
async deleteProduct(req,res){
    try {
        const product = await Products.findByPk(req.params.id);
        await product.destroy();
        
    } 
    catch (error) {
        res.status(402).json({
            error,
            data: null,
            "message":"failed to create"
        })
    }
}

}